EESchema Schematic File Version 4
LIBS:atelier-kicad-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6850 4050 2    50   Input ~ 0
RXD0
Text HLabel 6850 4250 2    50   Input ~ 0
TXD0
Text HLabel 4400 2050 0    50   Input ~ 0
ADC0
Text HLabel 6850 2050 2    50   Input ~ 0
ADC3
Text HLabel 8000 5250 2    50   Input ~ 0
Vin(5V)
Text HLabel 8000 4900 2    50   Input ~ 0
GND
Text Label 7800 4900 2    50   ~ 0
GND
Text Label 7800 5250 2    50   ~ 0
Vin(5V)
Wire Wire Line
	7800 5250 8000 5250
Wire Wire Line
	8000 4900 7800 4900
Text Label 6850 4650 0    50   ~ 0
GND
Text Label 6850 5050 0    50   ~ 0
Vin(5V)
Text Label 4400 3650 2    50   ~ 0
GND
Text HLabel 4400 2250 0    50   Input ~ 0
DAC0
Text HLabel 4400 3250 0    50   Input ~ 0
SCL
Text HLabel 4400 3050 0    50   Input ~ 0
SDA
NoConn ~ 6850 3850
NoConn ~ 6850 3650
NoConn ~ 6850 3450
NoConn ~ 6850 3250
NoConn ~ 6850 3050
NoConn ~ 6850 2850
NoConn ~ 6850 2650
NoConn ~ 6850 2450
NoConn ~ 6850 2250
NoConn ~ 4400 2650
NoConn ~ 4400 2850
Text Label 6850 4850 0    50   ~ 0
GND
Text Label 4400 3850 2    50   ~ 0
Vin(5V)
Text HLabel 4400 3450 0    50   Input ~ 0
3V3_0
NoConn ~ 6850 4450
Text HLabel 4400 2450 0    50   Input ~ 0
DAC1
$Comp
L atelier-kicad:Lolin32_OLED-fablab-pernes U?
U 1 1 5B48ECD7
P 5700 2750
AR Path="/5B48ECD7" Ref="U?"  Part="1" 
AR Path="/5B48B293/5B48ECD7" Ref="U3"  Part="1" 
F 0 "U3" H 5625 4265 50  0000 C CNN
F 1 "Lolin32_OLED" H 5625 4174 50  0000 C CNN
F 2 "ESP32:wemos lolin oled ESP32" H 5700 2750 50  0001 C CNN
F 3 "https://www.banggood.com/Wemos-ESP32-OLED-Module-For-Arduino-ESP32-OLED-WiFi-Bluetooth-Dual-ESP-32-ESP-32S-ESP8266-p-1148119.html" H 5700 2750 50  0001 C CNN
	1    5700 2750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
