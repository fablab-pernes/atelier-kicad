EESchema Schematic File Version 4
LIBS:atelier-kicad-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "Atelier Kicad"
Date "2018-12-19"
Rev "A"
Comp "FabLab Pernes-les-Fontaines"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L atelier-kicad:PWR_FLAG-power #FLG0101
U 1 1 5B465B2B
P 850 900
F 0 "#FLG0101" H 850 975 50  0001 C CNN
F 1 "PWR_FLAG" H 850 1074 50  0000 C CNN
F 2 "" H 850 900 50  0001 C CNN
F 3 "" H 850 900 50  0001 C CNN
	1    850  900 
	1    0    0    -1  
$EndComp
$Comp
L atelier-kicad:GND-power #PWR0101
U 1 1 5B465B8C
P 850 1050
F 0 "#PWR0101" H 850 800 50  0001 C CNN
F 1 "GND" H 850 900 50  0000 C CNN
F 2 "" H 850 1050 50  0001 C CNN
F 3 "" H 850 1050 50  0001 C CNN
	1    850  1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  900  850  1050
$Comp
L atelier-kicad:+5V-power #PWR0102
U 1 1 5B465BEF
P 1600 1050
F 0 "#PWR0102" H 1600 900 50  0001 C CNN
F 1 "+5V" H 1615 1223 50  0000 C CNN
F 2 "" H 1600 1050 50  0001 C CNN
F 3 "" H 1600 1050 50  0001 C CNN
	1    1600 1050
	-1   0    0    1   
$EndComp
$Comp
L atelier-kicad:PWR_FLAG-power #FLG0102
U 1 1 5B465C01
P 1600 900
F 0 "#FLG0102" H 1600 975 50  0001 C CNN
F 1 "PWR_FLAG" H 1600 1074 50  0000 C CNN
F 2 "" H 1600 900 50  0001 C CNN
F 3 "" H 1600 900 50  0001 C CNN
	1    1600 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 900  1600 1050
$Comp
L atelier-kicad:Conn_01x04-Connector_Generic J2
U 1 1 5B465FD8
P 9900 1450
F 0 "J2" H 9979 1442 50  0000 L CNN
F 1 "Button select(P)" H 9979 1351 50  0000 L CNN
F 2 "ESP32:HW4-2.0-90D" H 9900 1450 50  0001 C CNN
F 3 "~" H 9900 1450 50  0001 C CNN
F 4 "110990037" H 9900 1450 50  0001 C CNN "Seeedstudio PN"
	1    9900 1450
	1    0    0    -1  
$EndComp
$Comp
L atelier-kicad:Conn_01x04-Connector_Generic J3
U 1 1 5B4660B8
P 9900 2550
F 0 "J3" H 9979 2542 50  0000 L CNN
F 1 "RTC" H 9979 2451 50  0000 L CNN
F 2 "ESP32:HW4-2.0-90D" H 9900 2550 50  0001 C CNN
F 3 "~" H 9900 2550 50  0001 C CNN
F 4 "110990037" H 9900 2550 50  0001 C CNN "Seeedstudio PN"
	1    9900 2550
	1    0    0    -1  
$EndComp
$Comp
L atelier-kicad:Conn_01x04-Connector_Generic J4
U 1 1 5B4661DC
P 9900 3500
F 0 "J4" H 9979 3492 50  0000 L CNN
F 1 "Bluetooth 3.0" H 9979 3401 50  0000 L CNN
F 2 "ESP32:HW4-2.0-90D" H 9900 3500 50  0001 C CNN
F 3 "~" H 9900 3500 50  0001 C CNN
F 4 "110990037" H 9900 3500 50  0001 C CNN "Seeedstudio PN"
	1    9900 3500
	1    0    0    -1  
$EndComp
$Comp
L atelier-kicad:Conn_01x04-Connector_Generic J5
U 1 1 5B466288
P 9900 4450
F 0 "J5" H 9979 4442 50  0000 L CNN
F 1 "Light Sensor" H 9979 4351 50  0000 L CNN
F 2 "ESP32:HW4-2.0-90D" H 9900 4450 50  0001 C CNN
F 3 "~" H 9900 4450 50  0001 C CNN
F 4 "110990037" H 9900 4450 50  0001 C CNN "Seeedstudio PN"
	1    9900 4450
	1    0    0    -1  
$EndComp
$Comp
L atelier-kicad:R_Small-Device R10
U 1 1 5B4663FD
P 8050 4000
F 0 "R10" V 7950 4100 50  0000 C CNN
F 1 "47" V 7950 3900 50  0000 C CNN
F 2 "ESP32:R_0805" V 7980 4000 50  0001 C CNN
F 3 "~" H 8050 4000 50  0001 C CNN
F 4 "2502729" V 8050 4000 50  0001 C CNN "Farnell PN"
	1    8050 4000
	0    1    1    0   
$EndComp
$Comp
L atelier-kicad:R_Small-Device R11
U 1 1 5B4664FB
P 8050 4100
F 0 "R11" V 8150 4200 50  0000 C CNN
F 1 "47" V 8150 4000 50  0000 C CNN
F 2 "ESP32:R_0805" V 7980 4100 50  0001 C CNN
F 3 "~" H 8050 4100 50  0001 C CNN
F 4 "2502729" V 8050 4100 50  0001 C CNN "Farnell PN"
	1    8050 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	9550 3500 9700 3500
Wire Wire Line
	9700 3600 9550 3600
Text Label 9550 3500 2    50   ~ 0
Rx
Text Label 9550 3600 2    50   ~ 0
Tx
$Comp
L atelier-kicad:+3V3-power #PWR0104
U 1 1 5B466E3B
P 9550 3350
F 0 "#PWR0104" H 9550 3200 50  0001 C CNN
F 1 "+3V3" H 9550 3500 50  0000 C CNN
F 2 "" H 9550 3350 50  0001 C CNN
F 3 "" H 9550 3350 50  0001 C CNN
	1    9550 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 3350 9550 3400
Wire Wire Line
	9550 3400 9700 3400
$Comp
L atelier-kicad:GND-power #PWR0105
U 1 1 5B4672D7
P 9550 3750
F 0 "#PWR0105" H 9550 3500 50  0001 C CNN
F 1 "GND" H 9550 3600 50  0000 C CNN
F 2 "" H 9550 3750 50  0001 C CNN
F 3 "" H 9550 3750 50  0001 C CNN
	1    9550 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 3700 9550 3700
Wire Wire Line
	9550 3700 9550 3750
Wire Wire Line
	8100 5000 8250 5000
$Comp
L atelier-kicad:+3V3-power #PWR0106
U 1 1 5B467C87
P 9550 4300
F 0 "#PWR0106" H 9550 4150 50  0001 C CNN
F 1 "+3V3" H 9550 4450 50  0000 C CNN
F 2 "" H 9550 4300 50  0001 C CNN
F 3 "" H 9550 4300 50  0001 C CNN
	1    9550 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 4300 9550 4350
Wire Wire Line
	9550 4350 9700 4350
$Comp
L atelier-kicad:GND-power #PWR0107
U 1 1 5B467C8F
P 9550 4700
F 0 "#PWR0107" H 9550 4450 50  0001 C CNN
F 1 "GND" H 9550 4550 50  0000 C CNN
F 2 "" H 9550 4700 50  0001 C CNN
F 3 "" H 9550 4700 50  0001 C CNN
	1    9550 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 4650 9550 4650
Wire Wire Line
	9550 4650 9550 4700
$Comp
L atelier-kicad:R_Small-Device R9
U 1 1 5B468246
P 8000 5000
F 0 "R9" V 7900 5100 50  0000 C CNN
F 1 "47K" V 7900 4900 50  0000 C CNN
F 2 "ESP32:R_0805" V 7930 5000 50  0001 C CNN
F 3 "~" H 8000 5000 50  0001 C CNN
F 4 "1099812" V 8000 5000 50  0001 C CNN "Farnell PN"
	1    8000 5000
	0    1    1    0   
$EndComp
NoConn ~ 9700 4550
$Comp
L atelier-kicad:R_Small-Device R8
U 1 1 5B46863A
P 7800 5150
F 0 "R8" V 7700 5250 50  0000 C CNN
F 1 "10K" V 7700 5050 50  0000 C CNN
F 2 "ESP32:R_0805" V 7730 5150 50  0001 C CNN
F 3 "~" H 7800 5150 50  0001 C CNN
F 4 "2074334" V 7800 5150 50  0001 C CNN "Farnell PN"
	1    7800 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 5000 7800 5000
Wire Wire Line
	7800 5000 7800 5050
Wire Wire Line
	7800 5000 7350 5000
Connection ~ 7800 5000
$Comp
L atelier-kicad:GND-power #PWR0108
U 1 1 5B4688F3
P 7800 5300
F 0 "#PWR0108" H 7800 5050 50  0001 C CNN
F 1 "GND" H 7800 5150 50  0000 C CNN
F 2 "" H 7800 5300 50  0001 C CNN
F 3 "" H 7800 5300 50  0001 C CNN
	1    7800 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 5250 7800 5300
Text Label 9550 4450 2    50   ~ 0
ADC_In
Wire Wire Line
	9700 4450 9550 4450
Wire Wire Line
	9550 2550 9700 2550
Wire Wire Line
	9700 2650 9550 2650
Text Label 9550 2550 2    50   ~ 0
SDA
Text Label 9550 2650 2    50   ~ 0
SCL
$Comp
L atelier-kicad:+3V3-power #PWR0109
U 1 1 5B46A1D5
P 9550 2400
F 0 "#PWR0109" H 9550 2250 50  0001 C CNN
F 1 "+3V3" H 9550 2550 50  0000 C CNN
F 2 "" H 9550 2400 50  0001 C CNN
F 3 "" H 9550 2400 50  0001 C CNN
	1    9550 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 2400 9550 2450
Wire Wire Line
	9550 2450 9700 2450
$Comp
L atelier-kicad:GND-power #PWR0110
U 1 1 5B46A1DD
P 9550 2800
F 0 "#PWR0110" H 9550 2550 50  0001 C CNN
F 1 "GND" H 9550 2650 50  0000 C CNN
F 2 "" H 9550 2800 50  0001 C CNN
F 3 "" H 9550 2800 50  0001 C CNN
	1    9550 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 2750 9550 2750
Wire Wire Line
	9550 2750 9550 2800
Wire Wire Line
	9550 1450 9700 1450
Text Label 9550 1450 2    50   ~ 0
DAC_In1
$Comp
L atelier-kicad:+3V3-power #PWR0111
U 1 1 5B46AD45
P 9550 1300
F 0 "#PWR0111" H 9550 1150 50  0001 C CNN
F 1 "+3V3" H 9550 1450 50  0000 C CNN
F 2 "" H 9550 1300 50  0001 C CNN
F 3 "" H 9550 1300 50  0001 C CNN
	1    9550 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 1300 9550 1350
Wire Wire Line
	9550 1350 9700 1350
$Comp
L atelier-kicad:GND-power #PWR0112
U 1 1 5B46AD4D
P 9550 1700
F 0 "#PWR0112" H 9550 1450 50  0001 C CNN
F 1 "GND" H 9550 1550 50  0000 C CNN
F 2 "" H 9550 1700 50  0001 C CNN
F 3 "" H 9550 1700 50  0001 C CNN
	1    9550 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 1650 9550 1650
Wire Wire Line
	9550 1650 9550 1700
NoConn ~ 9700 1550
Text Label 8250 5000 0    50   ~ 0
ADC_In
Wire Wire Line
	8150 4000 8250 4000
Wire Wire Line
	8150 4100 8250 4100
Text Label 8250 4000 0    50   ~ 0
Rx
Text Label 8250 4100 0    50   ~ 0
Tx
$Comp
L atelier-kicad:R_Small-Device R13
U 1 1 5B46D37F
P 8100 3100
F 0 "R13" V 8000 3200 50  0000 C CNN
F 1 "47" V 8000 3000 50  0000 C CNN
F 2 "ESP32:R_0805" V 8030 3100 50  0001 C CNN
F 3 "~" H 8100 3100 50  0001 C CNN
F 4 "2502729" V 8100 3100 50  0001 C CNN "Farnell PN"
	1    8100 3100
	0    1    1    0   
$EndComp
$Comp
L atelier-kicad:R_Small-Device R14
U 1 1 5B46D41C
P 8100 3250
F 0 "R14" V 8200 3350 50  0000 C CNN
F 1 "47" V 8200 3150 50  0000 C CNN
F 2 "ESP32:R_0805" V 8030 3250 50  0001 C CNN
F 3 "~" H 8100 3250 50  0001 C CNN
F 4 "2502729" V 8100 3250 50  0001 C CNN "Farnell PN"
	1    8100 3250
	0    1    1    0   
$EndComp
$Comp
L atelier-kicad:R_Small-Device R12
U 1 1 5B46D922
P 8100 2000
F 0 "R12" V 8000 2100 50  0000 C CNN
F 1 "47" V 8000 1900 50  0000 C CNN
F 2 "ESP32:R_0805" V 8030 2000 50  0001 C CNN
F 3 "~" H 8100 2000 50  0001 C CNN
F 4 "2502729" V 8100 2000 50  0001 C CNN "Farnell PN"
	1    8100 2000
	0    1    1    0   
$EndComp
Wire Wire Line
	8200 2000 8300 2000
Text Label 8300 2000 0    50   ~ 0
DAC_In1
Wire Wire Line
	8200 3100 8300 3100
Wire Wire Line
	8200 3250 8300 3250
Text Label 8300 3250 0    50   ~ 0
SCL
Text Label 8300 3100 0    50   ~ 0
SDA
$Comp
L atelier-kicad:R_Small-Device R7
U 1 1 5B46F45F
P 7700 2850
F 0 "R7" H 7600 2900 50  0000 C CNN
F 1 "2K" H 7550 2800 50  0000 C CNN
F 2 "ESP32:R_0805" V 7630 2850 50  0001 C CNN
F 3 "~" H 7700 2850 50  0001 C CNN
F 4 "1469884" H 7700 2850 50  0001 C CNN "Farnell PN"
	1    7700 2850
	-1   0    0    1   
$EndComp
$Comp
L atelier-kicad:R_Small-Device R6
U 1 1 5B46F5C1
P 7500 2850
F 0 "R6" H 7400 2900 50  0000 C CNN
F 1 "2K" H 7350 2800 50  0000 C CNN
F 2 "ESP32:R_0805" V 7430 2850 50  0001 C CNN
F 3 "~" H 7500 2850 50  0001 C CNN
F 4 "1469884" H 7500 2850 50  0001 C CNN "Farnell PN"
	1    7500 2850
	1    0    0    -1  
$EndComp
$Comp
L atelier-kicad:+3V3-power #PWR0113
U 1 1 5B46F623
P 7600 2550
F 0 "#PWR0113" H 7600 2400 50  0001 C CNN
F 1 "+3V3" H 7600 2700 50  0000 C CNN
F 2 "" H 7600 2550 50  0001 C CNN
F 3 "" H 7600 2550 50  0001 C CNN
	1    7600 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 2750 7500 2650
Wire Wire Line
	7700 2650 7700 2750
Wire Wire Line
	7700 2950 7700 3100
Wire Wire Line
	7700 3100 8000 3100
Wire Wire Line
	7500 2950 7500 3250
Wire Wire Line
	7500 3250 8000 3250
Wire Wire Line
	7700 3100 7350 3100
Connection ~ 7700 3100
Wire Wire Line
	7500 3250 7350 3250
Connection ~ 7500 3250
Text Notes 9300 850  0    98   ~ 0
Grove Output
Wire Wire Line
	8000 2000 7350 2000
Wire Wire Line
	7950 4000 7350 4000
Wire Wire Line
	7950 4100 7350 4100
Wire Notes Line
	8650 1150 7000 1150
Wire Notes Line
	7000 1150 7000 5650
Wire Notes Line
	7000 5650 8650 5650
Wire Notes Line
	8650 5650 8650 1150
Text Notes 7200 1400 0    98   ~ 0
Signal calibration
$Comp
L atelier-kicad:+5V-power #PWR0114
U 1 1 5B481159
P 4150 6050
F 0 "#PWR0114" H 4150 5900 50  0001 C CNN
F 1 "+5V" H 4165 6223 50  0000 C CNN
F 2 "" H 4150 6050 50  0001 C CNN
F 3 "" H 4150 6050 50  0001 C CNN
	1    4150 6050
	1    0    0    -1  
$EndComp
$Comp
L atelier-kicad:LED_Small-Device D1
U 1 1 5B4817A9
P 4500 6150
F 0 "D1" H 4500 5945 50  0000 C CNN
F 1 "Yellow" H 4500 6036 50  0000 C CNN
F 2 "ESP32:LED_1206_HandSoldering" V 4500 6150 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2045944.pdf" V 4500 6150 50  0001 C CNN
F 4 "2099243" H 4500 6150 50  0001 C CNN "Farnell PN"
	1    4500 6150
	-1   0    0    1   
$EndComp
$Comp
L atelier-kicad:LED_Small-Device D2
U 1 1 5B481882
P 4500 6450
F 0 "D2" H 4500 6250 50  0000 C CNN
F 1 "Green" H 4500 6350 50  0000 C CNN
F 2 "ESP32:LED_1206_HandSoldering" V 4500 6450 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2045909.pdf" V 4500 6450 50  0001 C CNN
F 4 "1318243" H 4500 6450 50  0001 C CNN "Farnell PN"
	1    4500 6450
	1    0    0    -1  
$EndComp
$Comp
L atelier-kicad:GND-power #PWR0115
U 1 1 5B481A6B
P 4150 6550
F 0 "#PWR0115" H 4150 6300 50  0001 C CNN
F 1 "GND" H 4155 6377 50  0000 C CNN
F 2 "" H 4150 6550 50  0001 C CNN
F 3 "" H 4150 6550 50  0001 C CNN
	1    4150 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 6450 4150 6450
Wire Wire Line
	4150 6450 4150 6550
Wire Wire Line
	4150 6050 4150 6150
Wire Wire Line
	4150 6150 4400 6150
$Comp
L atelier-kicad:R_Small-Device R1
U 1 1 5B483CFF
P 4900 6150
F 0 "R1" V 4800 6050 50  0000 C CNN
F 1 "470" V 4800 6250 50  0000 C CNN
F 2 "ESP32:R_0805" V 4830 6150 50  0001 C CNN
F 3 "~" H 4900 6150 50  0001 C CNN
F 4 "1469932" V 4900 6150 50  0001 C CNN "Farnell PN"
	1    4900 6150
	0    1    1    0   
$EndComp
$Comp
L atelier-kicad:R_Small-Device R2
U 1 1 5B483EB4
P 4900 6450
F 0 "R2" V 4800 6550 50  0000 C CNN
F 1 "470" V 4800 6350 50  0000 C CNN
F 2 "ESP32:R_0805" V 4830 6450 50  0001 C CNN
F 3 "~" H 4900 6450 50  0001 C CNN
F 4 "1469932" V 4900 6450 50  0001 C CNN "Farnell PN"
	1    4900 6450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4600 6150 4800 6150
Wire Wire Line
	4600 6450 4800 6450
$Comp
L atelier-kicad:MCP73831-2-OT-Battery_Management U1
U 1 1 5B489114
P 5800 6050
F 0 "U1" H 5650 6300 50  0000 R CNN
F 1 "MCP73831-2-OT" H 6500 5750 50  0000 R CNN
F 2 "ESP32:SOT-23-5" H 5850 5800 50  0001 L CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001984g.pdf" H 5650 6000 50  0001 C CNN
F 4 "1332158" H 5800 6050 50  0001 C CNN "Farnell PN"
	1    5800 6050
	-1   0    0    -1  
$EndComp
$Comp
L atelier-kicad:+5V-power #PWR0116
U 1 1 5B48CBE6
P 5800 5350
F 0 "#PWR0116" H 5800 5200 50  0001 C CNN
F 1 "+5V" H 5815 5523 50  0000 C CNN
F 2 "" H 5800 5350 50  0001 C CNN
F 3 "" H 5800 5350 50  0001 C CNN
	1    5800 5350
	1    0    0    -1  
$EndComp
$Comp
L atelier-kicad:C_Small-Device C2
U 1 1 5B48CCA3
P 6250 5650
F 0 "C2" H 6342 5696 50  0000 L CNN
F 1 "10uF" H 6342 5605 50  0000 L CNN
F 2 "ESP32:R_1210_HandSoldering" H 6250 5650 50  0001 C CNN
F 3 "~" H 6250 5650 50  0001 C CNN
F 4 "1759469" H 6250 5650 50  0001 C CNN "Farnell PN"
	1    6250 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 5350 5800 5550
Wire Wire Line
	6250 5550 5800 5550
Connection ~ 5800 5550
Wire Wire Line
	5800 5550 5800 5750
$Comp
L atelier-kicad:GND-power #PWR0117
U 1 1 5B4902C2
P 6250 5800
F 0 "#PWR0117" H 6250 5550 50  0001 C CNN
F 1 "GND" H 6255 5627 50  0000 C CNN
F 2 "" H 6250 5800 50  0001 C CNN
F 3 "" H 6250 5800 50  0001 C CNN
	1    6250 5800
	1    0    0    -1  
$EndComp
$Comp
L atelier-kicad:R_Small-Device R3
U 1 1 5B4927BD
P 6400 6350
F 0 "R3" V 6300 6250 50  0000 C CNN
F 1 "2K" V 6300 6450 50  0000 C CNN
F 2 "ESP32:R_0805" V 6330 6350 50  0001 C CNN
F 3 "~" H 6400 6350 50  0001 C CNN
F 4 "1469884" V 6400 6350 50  0001 C CNN "Farnell PN"
	1    6400 6350
	-1   0    0    1   
$EndComp
Wire Wire Line
	6200 6150 6400 6150
Wire Wire Line
	5400 6150 5100 6150
Wire Wire Line
	5000 6450 5100 6450
Wire Wire Line
	5100 6450 5100 6150
Connection ~ 5100 6150
Wire Wire Line
	5100 6150 5000 6150
$Comp
L atelier-kicad:GND-power #PWR0118
U 1 1 5B49A7DE
P 5800 6550
F 0 "#PWR0118" H 5800 6300 50  0001 C CNN
F 1 "GND" H 5805 6377 50  0000 C CNN
F 2 "" H 5800 6550 50  0001 C CNN
F 3 "" H 5800 6550 50  0001 C CNN
	1    5800 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 6350 5800 6500
Wire Wire Line
	6400 6150 6400 6250
Wire Wire Line
	5800 6500 6400 6500
Wire Wire Line
	6400 6500 6400 6450
Connection ~ 5800 6500
Wire Wire Line
	5800 6500 5800 6550
Wire Wire Line
	6250 5750 6250 5800
Wire Wire Line
	5400 5950 5400 5350
$Comp
L atelier-kicad:C_Small-Device C1
U 1 1 5B4AA6F2
P 5050 5550
F 0 "C1" H 5142 5596 50  0000 L CNN
F 1 "10uF" H 5142 5505 50  0000 L CNN
F 2 "ESP32:R_1210_HandSoldering" H 5050 5550 50  0001 C CNN
F 3 "~" H 5050 5550 50  0001 C CNN
F 4 "1759469" H 5050 5550 50  0001 C CNN "Farnell PN"
	1    5050 5550
	1    0    0    -1  
$EndComp
$Comp
L atelier-kicad:GND-power #PWR0119
U 1 1 5B4AA76E
P 5050 5700
F 0 "#PWR0119" H 5050 5450 50  0001 C CNN
F 1 "GND" H 5055 5527 50  0000 C CNN
F 2 "" H 5050 5700 50  0001 C CNN
F 3 "" H 5050 5700 50  0001 C CNN
	1    5050 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 5650 5050 5700
Wire Wire Line
	5050 5450 5050 5350
Wire Wire Line
	5050 5350 5400 5350
$Comp
L atelier-kicad:Polyfuse_Small-Device F1
U 1 1 5B4B184B
P 4600 5350
F 0 "F1" V 4395 5350 50  0000 C CNN
F 1 "Polyfuse 0ZCG0050AF2C (500mA 1A)" V 4300 6000 50  0000 C CNN
F 2 "ESP32:R_1812_HandSoldering" H 4650 5150 50  0001 L CNN
F 3 "http://www.farnell.com/datasheets/2571640.pdf" H 4600 5350 50  0001 C CNN
F 4 "2834905" V 4600 5350 50  0001 C CNN "Farnell PN"
	1    4600 5350
	0    1    1    0   
$EndComp
Wire Wire Line
	4700 5350 4850 5350
Connection ~ 5050 5350
Wire Wire Line
	5050 5350 5050 5200
Text Label 5050 5200 0    50   ~ 0
VBat
$Comp
L atelier-kicad:Conn_01x02-Connector_Generic J1
U 1 1 5B4B5E4B
P 4050 5450
F 0 "J1" H 3970 5125 50  0000 C CNN
F 1 "S2B-PH-SM4-TB" H 3970 5216 50  0000 C CNN
F 2 "ESP32:JST_PH_S2B-PH-SM4-TB_1x02-1MP_P2.00mm_Horizontal" H 4050 5450 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1699381.pdf" H 4050 5450 50  0001 C CNN
F 4 "9492615" H 4050 5450 50  0001 C CNN "Farnell PN"
	1    4050 5450
	-1   0    0    1   
$EndComp
Wire Wire Line
	4250 5350 4500 5350
$Comp
L atelier-kicad:GND-power #PWR0120
U 1 1 5B4B7D79
P 4400 5550
F 0 "#PWR0120" H 4400 5300 50  0001 C CNN
F 1 "GND" H 4405 5377 50  0000 C CNN
F 2 "" H 4400 5550 50  0001 C CNN
F 3 "" H 4400 5550 50  0001 C CNN
	1    4400 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 5450 4400 5450
Wire Wire Line
	4400 5450 4400 5550
Wire Notes Line
	3800 4700 6750 4700
Wire Notes Line
	6750 4700 6750 6950
Wire Notes Line
	6750 6950 3800 6950
Wire Notes Line
	3800 6950 3800 4700
Text Notes 4450 4900 0    98   ~ 0
Battery Management
$Comp
L atelier-kicad:SW_SPST-Switch SW1
U 1 1 5B4C2FA8
P 4500 3550
F 0 "SW1" H 4500 3785 50  0000 C CNN
F 1 "OS102011MA1QN1" H 4500 3694 50  0000 C CNN
F 2 "ESP32:SW_CuK_OS102011MA1QN1_SPDT_Angled" H 4500 3550 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2674787.pdf" H 4500 3550 50  0001 C CNN
F 4 "1201431" H 4500 3550 50  0001 C CNN "Farnell PN"
	1    4500 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3550 4100 3550
Text Label 4100 3550 2    50   ~ 0
VBat
Wire Notes Line
	3800 2850 6750 2850
Text Notes 4900 3050 0    98   ~ 0
3V3 power
$Comp
L atelier-kicad:TLV27L2IDGR4-fablab-pernes U2
U 1 1 5B4DBA62
P 5550 1950
F 0 "U2" H 5550 2325 50  0000 C CNN
F 1 "LMV358SG-13" H 5550 2234 50  0000 C CNN
F 2 "ESP32:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5550 1950 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2100161.pdf" H 5550 1950 50  0001 C CNN
F 4 "1904047" H 5550 1950 50  0001 C CNN "Farnell PN"
	1    5550 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1900 6100 1900
Wire Wire Line
	6100 1900 6100 2000
Wire Wire Line
	6100 2000 5950 2000
Wire Wire Line
	5150 1800 5000 1800
Wire Wire Line
	5000 1800 5000 1900
Wire Wire Line
	5000 1900 5150 1900
Wire Wire Line
	5950 1800 6100 1800
$Comp
L atelier-kicad:GND-power #PWR0124
U 1 1 5B4EA332
P 5000 2450
F 0 "#PWR0124" H 5000 2200 50  0001 C CNN
F 1 "GND" H 5005 2277 50  0000 C CNN
F 2 "" H 5000 2450 50  0001 C CNN
F 3 "" H 5000 2450 50  0001 C CNN
	1    5000 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2100 5000 2100
Wire Wire Line
	5000 2100 5000 2350
$Comp
L atelier-kicad:R_Small-Device R4
U 1 1 5B4ED415
P 4200 2000
F 0 "R4" V 4100 1900 50  0000 C CNN
F 1 "47K" V 4100 2100 50  0000 C CNN
F 2 "ESP32:R_0805" V 4130 2000 50  0001 C CNN
F 3 "~" H 4200 2000 50  0001 C CNN
F 4 "1099812" V 4200 2000 50  0001 C CNN "Farnell PN"
	1    4200 2000
	0    1    1    0   
$EndComp
$Comp
L atelier-kicad:R_Small-Device R5
U 1 1 5B4ED7A4
P 4750 2200
F 0 "R5" H 4650 2150 50  0000 C CNN
F 1 "10K" H 4600 2250 50  0000 C CNN
F 2 "ESP32:R_0805" V 4680 2200 50  0001 C CNN
F 3 "~" H 4750 2200 50  0001 C CNN
F 4 "2074334" H 4750 2200 50  0001 C CNN "Farnell PN"
	1    4750 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	4750 2300 4750 2350
Wire Wire Line
	4750 2350 5000 2350
Connection ~ 5000 2350
Wire Wire Line
	5000 2350 5000 2450
Wire Wire Line
	4300 2000 4550 2000
Wire Wire Line
	4750 2100 4750 2000
Wire Wire Line
	4750 2000 5150 2000
Text Label 4000 2000 2    50   ~ 0
VBat
Wire Wire Line
	4000 2000 4100 2000
Wire Wire Line
	5000 1800 4800 1800
Connection ~ 5000 1800
Wire Wire Line
	6100 1900 6300 1900
Connection ~ 6100 1900
Text Label 5950 2100 0    50   ~ 0
+In
Text Label 7350 5000 2    50   ~ 0
+In
Text Label 6300 1900 0    50   ~ 0
ADC0
Text Label 4800 1800 2    50   ~ 0
ADC3
Wire Notes Line
	3800 1150 6750 1150
Wire Notes Line
	6750 1150 6750 2700
Wire Notes Line
	6750 2700 3800 2700
Wire Notes Line
	3800 2700 3800 1150
$Sheet
S 950  1700 900  1400
U 5B48B293
F0 "Lolin32 OLED" 50
F1 "lolin32_oled.sch" 50
F2 "RXD0" I R 1850 2250 50 
F3 "TXD0" I R 1850 2400 50 
F4 "ADC0" I R 1850 2550 50 
F5 "ADC3" I R 1850 2700 50 
F6 "Vin(5V)" I L 950 1800 50 
F7 "GND" I L 950 3000 50 
F8 "DAC0" I R 1850 2850 50 
F9 "SCL" I R 1850 2100 50 
F10 "SDA" I R 1850 1950 50 
F11 "3V3_0" I R 1850 1800 50 
F12 "DAC1" I R 1850 3000 50 
$EndSheet
Text Label 1850 2250 0    50   ~ 0
TXD0
Text Label 1850 2400 0    50   ~ 0
RXD0
Text Label 1850 2550 0    50   ~ 0
ADC0
Text Label 1850 2700 0    50   ~ 0
ADC3
Text Label 1850 2850 0    50   ~ 0
DAC0
Text Label 7350 4100 2    50   ~ 0
TXD0
Text Label 7350 4000 2    50   ~ 0
RXD0
Text Label 7350 2000 2    50   ~ 0
DAC0
$Comp
L atelier-kicad:GND-power #PWR0125
U 1 1 5B499AF4
P 800 3100
F 0 "#PWR0125" H 800 2850 50  0001 C CNN
F 1 "GND" H 805 2927 50  0000 C CNN
F 2 "" H 800 3100 50  0001 C CNN
F 3 "" H 800 3100 50  0001 C CNN
	1    800  3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  3000 800  3000
Wire Wire Line
	800  3000 800  3100
$Comp
L atelier-kicad:+5V-power #PWR0126
U 1 1 5B49D591
P 800 1700
F 0 "#PWR0126" H 800 1550 50  0001 C CNN
F 1 "+5V" H 815 1873 50  0000 C CNN
F 2 "" H 800 1700 50  0001 C CNN
F 3 "" H 800 1700 50  0001 C CNN
	1    800  1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  1800 800  1800
Wire Wire Line
	800  1800 800  1700
Text Label 7350 3100 2    50   ~ 0
SDA0
Text Label 7350 3250 2    50   ~ 0
SCL0
Text Label 1850 1950 0    50   ~ 0
SDA0
Text Label 1850 2100 0    50   ~ 0
SCL0
$Comp
L atelier-kicad:+3V3-power #PWR0127
U 1 1 5B4ECA70
P 2000 1700
F 0 "#PWR0127" H 2000 1550 50  0001 C CNN
F 1 "+3V3" H 2015 1873 50  0000 C CNN
F 2 "" H 2000 1700 50  0001 C CNN
F 3 "" H 2000 1700 50  0001 C CNN
	1    2000 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 1800 2000 1800
Wire Wire Line
	2000 1800 2000 1700
Wire Bus Line
	500  7750 3350 7750
Wire Bus Line
	3350 7750 3350 6750
Wire Bus Line
	3350 6750 500  6750
Wire Bus Line
	500  6750 500  7750
Text Notes 600  7450 0    50   ~ 0
- ajout d'un BP pour navigation plus aisée\n- TODO comment ajouter datasheet PDF en local\n- TODO dessiner empreintes\n- DAC in seocnd BP (valid)\n- changer symbole Q1 en PMOS\n- modifier valeurs resistances (par défaut à 47 ohm) ?\n- compléter brochage ESP32\n- inverser RX et TX ?
Text Notes 550  6700 0    50   ~ 0
Commentaires de révision et //TODO
$Comp
L atelier-kicad:Conn_01x04-Connector_Generic J6
U 1 1 5C1C2D99
P 9900 5400
F 0 "J6" H 9979 5392 50  0000 L CNN
F 1 "Button valid (P)" H 9979 5301 50  0000 L CNN
F 2 "ESP32:HW4-2.0-90D" H 9900 5400 50  0001 C CNN
F 3 "~" H 9900 5400 50  0001 C CNN
F 4 "110990037" H 9900 5400 50  0001 C CNN "Seeedstudio PN"
	1    9900 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 5400 9700 5400
$Comp
L atelier-kicad:+3V3-power #PWR0103
U 1 1 5C1C2DA1
P 9550 5250
F 0 "#PWR0103" H 9550 5100 50  0001 C CNN
F 1 "+3V3" H 9550 5400 50  0000 C CNN
F 2 "" H 9550 5250 50  0001 C CNN
F 3 "" H 9550 5250 50  0001 C CNN
	1    9550 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 5250 9550 5300
Wire Wire Line
	9550 5300 9700 5300
$Comp
L atelier-kicad:GND-power #PWR0128
U 1 1 5C1C2DA9
P 9550 5650
F 0 "#PWR0128" H 9550 5400 50  0001 C CNN
F 1 "GND" H 9550 5500 50  0000 C CNN
F 2 "" H 9550 5650 50  0001 C CNN
F 3 "" H 9550 5650 50  0001 C CNN
	1    9550 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 5600 9550 5600
Wire Wire Line
	9550 5600 9550 5650
NoConn ~ 9700 5500
Wire Notes Line
	10750 6000 10750 600 
Wire Notes Line
	8900 600  8900 6000
Wire Notes Line
	10700 600  8900 600 
Wire Notes Line
	8900 6000 10750 6000
Text Notes 3950 1350 0    98   ~ 0
Etage buffer ADC
$Comp
L atelier-kicad:R_Small-Device R15
U 1 1 5C805B2C
P 8100 2250
F 0 "R15" V 8000 2350 50  0000 C CNN
F 1 "47" V 8000 2150 50  0000 C CNN
F 2 "ESP32:R_0805" V 8030 2250 50  0001 C CNN
F 3 "~" H 8100 2250 50  0001 C CNN
F 4 "2502729" V 8100 2250 50  0001 C CNN "Farnell PN"
	1    8100 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	8200 2250 8300 2250
Text Label 8300 2250 0    50   ~ 0
DAC_In2
Wire Wire Line
	8000 2250 7350 2250
Text Label 7350 2250 2    50   ~ 0
DAC1
Text Label 9550 5400 2    50   ~ 0
DAC_In2
Text Label 1850 3000 0    50   ~ 0
DAC1
Wire Wire Line
	7500 2650 7600 2650
$Comp
L atelier-kicad:C_Small-Device C3
U 1 1 5CD4084B
P 6400 1500
F 0 "C3" H 6492 1546 50  0000 L CNN
F 1 "100nF" H 6492 1455 50  0000 L CNN
F 2 "ESP32:C_0805" H 6400 1500 50  0001 C CNN
F 3 "~" H 6400 1500 50  0001 C CNN
F 4 "1759469" H 6400 1500 50  0001 C CNN "Farnell PN"
	1    6400 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 1400 6100 1800
Wire Wire Line
	6400 1400 6100 1400
$Comp
L atelier-kicad:GND-power #PWR01
U 1 1 5CD4DA09
P 6400 1650
F 0 "#PWR01" H 6400 1400 50  0001 C CNN
F 1 "GND" H 6550 1600 50  0000 C CNN
F 2 "" H 6400 1650 50  0001 C CNN
F 3 "" H 6400 1650 50  0001 C CNN
	1    6400 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 1600 6400 1650
$Comp
L atelier-kicad:C_Small-Device C4
U 1 1 5CD5314D
P 4550 2200
F 0 "C4" H 4350 2250 50  0000 L CNN
F 1 "100nF" H 4250 2150 50  0000 L CNN
F 2 "ESP32:C_0805" H 4550 2200 50  0001 C CNN
F 3 "~" H 4550 2200 50  0001 C CNN
F 4 "1759469" H 4550 2200 50  0001 C CNN "Farnell PN"
	1    4550 2200
	1    0    0    -1  
$EndComp
Connection ~ 4750 2000
Wire Wire Line
	4550 2300 4550 2350
Wire Wire Line
	4550 2350 4750 2350
Connection ~ 4750 2350
Wire Wire Line
	4550 2100 4550 2000
Connection ~ 4550 2000
Wire Wire Line
	4550 2000 4750 2000
Wire Wire Line
	7600 2550 7600 2650
Connection ~ 7600 2650
Wire Wire Line
	7600 2650 7700 2650
$Comp
L atelier-kicad:+3V3-power #PWR0121
U 1 1 5D2D9DCF
P 6100 1350
F 0 "#PWR0121" H 6100 1200 50  0001 C CNN
F 1 "+3V3" H 6115 1523 50  0000 C CNN
F 2 "" H 6100 1350 50  0001 C CNN
F 3 "" H 6100 1350 50  0001 C CNN
	1    6100 1350
	1    0    0    -1  
$EndComp
Connection ~ 6100 1400
$Comp
L atelier-kicad:+3V3-power #PWR0123
U 1 1 5D2DF011
P 6450 3400
F 0 "#PWR0123" H 6450 3250 50  0001 C CNN
F 1 "+3V3" H 6465 3573 50  0000 C CNN
F 2 "" H 6450 3400 50  0001 C CNN
F 3 "" H 6450 3400 50  0001 C CNN
	1    6450 3400
	1    0    0    -1  
$EndComp
$Comp
L atelier-kicad:C_Small-Device C5
U 1 1 5D2F5DCB
P 5750 4050
F 0 "C5" H 5842 4096 50  0000 L CNN
F 1 "10uF" H 5842 4005 50  0000 L CNN
F 2 "ESP32:R_1210_HandSoldering" H 5750 4050 50  0001 C CNN
F 3 "~" H 5750 4050 50  0001 C CNN
F 4 "1759469" H 5750 4050 50  0001 C CNN "Farnell PN"
	1    5750 4050
	1    0    0    -1  
$EndComp
$Comp
L atelier-kicad:GND-power #PWR0129
U 1 1 5D2FAAEE
P 5750 4250
F 0 "#PWR0129" H 5750 4000 50  0001 C CNN
F 1 "GND" H 5755 4077 50  0000 C CNN
F 2 "" H 5750 4250 50  0001 C CNN
F 3 "" H 5750 4250 50  0001 C CNN
	1    5750 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4150 5750 4250
$Comp
L atelier-kicad:D_Schottky_Small-Device D4
U 1 1 5D304CF2
P 4850 5550
F 0 "D4" H 4850 5345 50  0000 C CNN
F 1 "D_Schottky 40V 3A" H 4850 5436 25  0000 C CNN
F 2 "ESP32:D_SMA_Handsoldering" V 4850 5550 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1750836.pdf" V 4850 5550 50  0001 C CNN
F 4 "1431078" H 4850 5550 50  0001 C CNN "Farnell PN"
	1    4850 5550
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 5450 4850 5350
Connection ~ 4850 5350
Wire Wire Line
	4850 5350 5050 5350
Wire Wire Line
	4850 5650 4850 5700
Wire Wire Line
	4850 5700 5050 5700
Connection ~ 5050 5700
Wire Notes Line
	6750 2850 6750 4500
Wire Notes Line
	3800 2850 3800 4500
$Comp
L Regulator_Linear:AP2112K-3.3 U4
U 1 1 5D33E973
P 6100 3650
F 0 "U4" H 6100 3992 50  0000 C CNN
F 1 "AP2112K-3.3" H 6100 3901 50  0000 C CNN
F 2 "ESP32:SOT-23-5" H 6100 3975 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/AP2112.pdf" H 6100 3750 50  0001 C CNN
	1    6100 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3650 5750 3650
$Comp
L atelier-kicad:GND-power #PWR0130
U 1 1 5D35010B
P 6100 4250
F 0 "#PWR0130" H 6100 4000 50  0001 C CNN
F 1 "GND" H 6105 4077 50  0000 C CNN
F 2 "" H 6100 4250 50  0001 C CNN
F 3 "" H 6100 4250 50  0001 C CNN
	1    6100 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3650 5750 3550
Connection ~ 5750 3550
Wire Wire Line
	5750 3550 5800 3550
Connection ~ 5750 3650
Wire Wire Line
	6400 3550 6450 3550
Wire Wire Line
	6450 3550 6450 3400
$Comp
L atelier-kicad:C_Small-Device C6
U 1 1 5D372915
P 6450 4050
F 0 "C6" H 6542 4096 50  0000 L CNN
F 1 "10uF" H 6542 4005 50  0000 L CNN
F 2 "ESP32:R_1210_HandSoldering" H 6450 4050 50  0001 C CNN
F 3 "~" H 6450 4050 50  0001 C CNN
F 4 "1759469" H 6450 4050 50  0001 C CNN "Farnell PN"
	1    6450 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3950 6450 3550
Connection ~ 6450 3550
Wire Wire Line
	6100 4250 6100 3950
$Comp
L atelier-kicad:GND-power #PWR0131
U 1 1 5D37E732
P 6450 4250
F 0 "#PWR0131" H 6450 4000 50  0001 C CNN
F 1 "GND" H 6455 4077 50  0000 C CNN
F 2 "" H 6450 4250 50  0001 C CNN
F 3 "" H 6450 4250 50  0001 C CNN
	1    6450 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 4150 6450 4250
Wire Notes Line
	3800 4500 6750 4500
Wire Wire Line
	6100 1400 6100 1350
Wire Wire Line
	5750 3650 5750 3950
Wire Wire Line
	4700 3550 5750 3550
$EndSCHEMATC
